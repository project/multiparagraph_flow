## Development
### Compile and format CSS
The styling for this module is written in SCSS, and must be compiled to CSS.

If you intend to contribute changes to the CSS, please first edit src/global/grid.scss

The CSS must also be formatted to meet Drupal coding standards. Please ensure that sassc and prettier are installed. After making changes to the scss file, to compile and format the CSS code, run:

`sassc src/global/grid.scss dist/global/grid.css && prettier --write dist/global/grid.css`
